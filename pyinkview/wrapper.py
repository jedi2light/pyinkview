"""
pyinkview wrapper module
"""

import os
from ctypes import *

sys = "/ebrmain/lib/libinkview.so"
local = "./libraries/libinkview.so"

ink = CDLL(local
           if os.path.exists(local) else sys)


def create_handler(func):

    """
    Creates 'InkViewMain()' events handler
    """

    return CFUNCTYPE(c_int, c_int, c_int, c_int)(func)
