#include <string>
#include <cstring>

#include <inkui.hpp>

#define GREETINGS_FONT_SIZE 60
#define PRIMARY_FONT_SIZE 40
#define SECONDARY_FONT_SIZE 25
#define GROUP_VIEW_V_SPACING 50

struct coordinate {
    int x;
    int y;
};
typedef coordinate coordinate;

class MyScene : public InkUI::Scene {
private:
    coordinate*  touch_coordinate; 
public:
    InkUI::View* view();
    void         curtains();
    void         set_touch_coordinate(int x,
                                      int y);
    std::string  get_touch_coordinate_string();
};

void MyScene::curtains() {
    CloseApp();
}

void MyScene::set_touch_coordinate(
    int x,
    int y
) {
    this->touch_coordinate = new coordinate;
    this->touch_coordinate->x = x;
    this->touch_coordinate->y = y;
}

std::string MyScene::get_touch_coordinate_string() {
    if (this->touch_coordinate) {
        int x = this->touch_coordinate->x;
        int y = this->touch_coordinate->y;
        std::string formatted = std::to_string(x)
                                + "x"
                                + std::to_string(y);
        return formatted;
    } else {
        return "Waiting for a touch to be recevied";
    }
}

InkUI::View* MyScene::view () {
    // We have to keep the method result here
    // in order to copy 'c_str()' using 'strcpy()'
    std::string formatted =
                this->get_touch_coordinate_string();
    return InkUI::Group::Default({
        InkUI::Text::Default(
            "Hello Pocketbook from InkUI library!"
        )->with_font_size(GREETINGS_FONT_SIZE),
        InkUI::Text::Default(
            // I hate that we couldn't use string!
            strcpy(new char[formatted.size() + 1],
                   formatted.c_str())
        )->with_font_size(PRIMARY_FONT_SIZE),
        InkUI::Rect::Default(), // TODO: parametrize
        InkUI::Text::Default(
            "Press any key to exit this InkUI demo."
        )->with_font_size(SECONDARY_FONT_SIZE)
    })->with_vertical_spacing(GROUP_VIEW_V_SPACING);
}

int inkview_main_handler (int event_type,
                          int first_argument = 0,
                          int second_argument = 0) {
    static MyScene* scene = new MyScene();
    if (event_type == EVT_SHOW) {
        // Call render() on EVT_SHOW response
       scene->render();
    } else if (event_type == EVT_KEYPRESS) {
       // Close the application and ... Curtains!
       scene->curtains();
    } else if (event_type == EVT_POINTERDOWN) {
       // Set the recevied touch event coordinate
       scene->set_touch_coordinate(first_argument,
                                   second_argument);
       scene->render(); // call render() to show the
       // recevied a touch event coordinate: X and Y
    }
    return 0;
}

int main() {
    InkViewMain(inkview_main_handler); // event loop
    return 0;
}
