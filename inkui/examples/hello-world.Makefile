# Both for Pocketbook and Emulator (libwine)

# Pocketbook SDK
SDK              = ../SDK_6.3.0/SDK-B288
SDK_CC           = $(SDK)/usr/bin/arm-obreey-linux-gnueabi-gcc
SDK_CXX          = $(SDK)/usr/bin/arm-obreey-linux-gnueabi-g++
SDK_LD_FLAGS     = -linkview
SDK_CC_FLAGS     = -I ../lib

# Emulator (libwine)
EMU_CC           = winegcc
EMU_CXX          = wineg++
EMU_CC_FLAGS     = -I/usr/local/pocketbook/include \
                   -I/usr/include/freetype2 \
                   -Wall -fomit-frame-pointer -fpermissive -Wno-narrowing \
                   -O2 -mwindows -m32 -I../lib
EMU_LD_FLAGS     = -L/usr/local/pocketbook/lib -lfreetype -lz -ljpeg -linkview

# Project properties
EXECUTABLE       = hello-world
EXECUTABLE_SO    = $(EXECUTABLE).exe.so
EXECUTABLE_EXE   = $(EXECUTABLE).exe
EXECUTABLE_APP   = $(EXECUTABLE).app

# Project sources
SOURCES	        += hello-world.cpp

app: $(SOURCES)
	LD_LIBRARY_PATH=$(SDK)/usr/lib \
	$(SDK_CXX) $(SOURCES) $(SDK_CC_FLAGS) $(SDK_LD_FLAGS) -o $(EXECUTABLE_APP)

exe: $(SOURCES)
	$(EMU_CXX) $(SOURCES) $(EMU_CC_FLAGS) $(EMU_LD_FLAGS) -o $(EXECUTABLE)

run: exe
	WINEARCH=win32 WINEPREFIX=$(HOME)/.wine32 wine $(EXECUTABLE_SO)

run-strace: exe
	WINEARCH=win32 WINEPREFIX=$(HOME)/.wine32 strace wine $(EXECUTABLE_SO)

clean:
	rm $(EXECUTABLE_APP) || true
	rm *.o || true; rm $(EXECUTABLE_SO) || true; rm $(EXECUTABLE_EXE) || true
