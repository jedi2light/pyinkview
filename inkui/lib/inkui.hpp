#include <vector>
#include <cstdarg>

#include <iostream>

#include <inkview.h>

namespace InkUI {

class View {
public:
    virtual int   preferrable_height() = 0;
    virtual void  render(int x = 0,  int y = 0,
                         int w = -1, int h = -1) = 0;
};

class Scene {
private:
    bool          did_full_update; // track FullUpdate
public:
    virtual View* view() = 0;

    Scene() {
        // .. later on, determine what to do on init()
    }

    void render() {
        ClearScreen(); // clear current buffer content

        if (this->did_full_update) {
            // TODO: figure out how to fix this bug :(
            this->view()->render(
                0, 0, -1, ScreenHeight() - 210 // hack
            );
            SoftUpdate(); // draw what's inside buffer
        } else {
            this->view()->render(); // casual render()
            FullUpdate(); // draw what's inside buffer
            this->did_full_update = true; //softupdate
        }
    }
};

class Rect : public View {
private:
    // TODO: parametrize
public:
    static Rect* Default(
    ) {
        return new Rect();
    }
    Rect()
    {}

    int preferrable_height() {
        return 50; // TODO: return parametrized
    }

    void render(
        int x = 0,      int y = 0,
        int width = -1, int height = -1
    ) {
        if (width == -1) width = ScreenWidth();
        if (height == -1) height = ScreenHeight();

        DrawRect(x, y, width, 50, BLACK); // stub!
        FillArea(x, y, width, 50, BLACK); // stub!
    }
};

class Text : public View {
private:
    const char*  value;
    int          align;
    const char*  font_name;
    int          font_size;

public:
    static Text* Default(
        const char*  value
    ) {
        return new Text(value);
    }

    Text(
        const char*  value
    ) {
        this->value = value;
        this->align = ALIGN_CENTER;
        this->font_name = "LiberationSans";
        this->font_size = 40;
    }

    Text(
        const char*  value,
        int          custom_align,
        const char*  custom_font_name,
        int          custom_font_size
    ) {
        this->value = value;
        this->align = custom_align;
        this->font_name = custom_font_name;
        this->font_size = custom_font_size;
    }

    Text* with_align(
        int          custom_align
    ) {
        return new Text(this->value,
                        custom_align,
                        this->font_name,
                        this->font_size);
    }

    Text* with_font_name(
        const char*  custom_font_name
    ) {
        return new Text(this->value,
                        this->align,
                        custom_font_name,
                        this->font_size);
    }

    Text* with_font_size(
        int          custom_font_size
    ) {
        return new Text(this->value,
                        this->align,
                        this->font_name,
                        custom_font_size);
    }

    int preferrable_height() {
        return this->font_size;
    }

    void render(
        int x = 0,      int y = 0,
        int width = -1, int height = -1
    ) {

        ifont* font = OpenFont(this->font_name,
                               this->font_size, 0);
        SetFont(font, BLACK);

        if (width == -1) width = ScreenWidth();
        if (height == -1) height = ScreenHeight();

        DrawTextRect(
            x, y,
            width, height, this->value, this->align
        );
    }
};

class Group : public View {
private:
    std::vector<View*> views;
    int                v_spacing;

public:
    static Group* Default(
        const  std::vector<View*> views
    ) {
        return new Group(views);
    };

    Group(
        const  std::vector<View*> views
    ) {
        this->views = views;
        this->v_spacing = 10; // default to 10 px
    }

    Group(
        const  std::vector<View*> views,
        int    custom_vertical_spacing
    ) {
        this->views = views;
        this->v_spacing = custom_vertical_spacing;
    }

    Group* with_vertical_spacing(
        int    custom_vertical_spacing
    ) {
        return new Group(this->views,
                         custom_vertical_spacing);
    }

    int preferrable_height() {
        int overall_preferrable_height = 0;
        for (int i = 0; i < this->views.size(); ++i) {
            overall_preferrable_height +=
                 this->v_spacing +
                 this->views[i]->preferrable_height();
        }
        return overall_preferrable_height;
    }

    void render(
        int x = 0,      int y = 0,
        int width = -1, int height = -1
    ) {

        if (width == -1) width = ScreenWidth();
        if (height == -1) height = ScreenHeight();

        int top_boundary = (height
                - this->preferrable_height()) / 2;

        for (int i = 0; i < this->views.size(); ++i) {
            View* view = this->views[i];
            view->render(x,
                         top_boundary,
                         width,
                         view->preferrable_height());
            top_boundary+= this->v_spacing +
                           view->preferrable_height();
        }
    }
};

};
