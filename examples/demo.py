#!/mnt/ext1/system/bin/python3

import os
import sys
from pyinkview import defines as defs
from pyinkview.wrapper import ink, create_handler


def main_handler(event_type: int,
                 first_argument: int = 0,
                 second_argument: int = 0) -> int:

    if event_type == defs.EVT_KEYPRESS:
        # Press any key to exit this PyInkview demo
        ink.CloseApp()

    elif event_type == defs.EVT_INIT: # first event
        # Open LiberationSans font with size of 40pt
        font = ink.OpenFont("LiberationSans", 40, 0)
        ink.SetFont(font, defs.BLACK)

        # - Flush the bufffer by ClearScreen()
        # - Draw a single text rectangle to the buffer
        # - Release application buffer content to screen
        ink.ClearScreen
        ink.DrawTextRect(0,
                         int(ink.ScreenHeight()/2) - 20,
                         ink.ScreenWidth(),
                         40,
                         b'Hello, World',
                         defs.ALIGN_CENTER)
        ink.FullUpdate()

        # Free previously opened font from device memory
        ink.CloseFont(font)

    return 0


def main() -> int:

    return ink.InkViewMain(create_handler(main_handler))


if __name__ == '__main__':

    sys.exit(main())
