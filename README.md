# Contents

1. **PyInkview** is the Python 3.6 wrapper for PocketBook `libinkview` library using builtin Python 3.6 **FFI API** via `ctypes` module.
2. **InkUI** is the small one-header-file library in C++ that mimics SwiftUI in certain ways and allows to write GUI in a declarative way.

# PyInkview

## Install Python

Refer to the <https://gitlab.com/jedi2light/PB-E614.4.4.1774-Packages> to fetch pre-built Python 3.6 binaries.

## Install PyInkview

1. Go the <https://gitlab.com/jedi2light/pyinkview/-/artifacts>, fetch the most recent `artifacts.zip`, then unpack it.
2. Unpack the `pyinkview-any.tar.gz` archive onto your PocketBook device **connected and mounted** to your **host pc**.

## Usage of Pyinkview

See [Python Demo](examples/demo.py)  
[This](https://gitlab.com/jedi2light/2048) 2048 Python Prorotype has implementation for PocketBook platform using **PyInkview** library!

# InkUI

## Demo Screenshot

![alt=DemoScreenshot](./inkui/examples/hello-world.png)

## Fetch SDK and Compile a Hello World demo application

```bash
cd inkui && ./bin/fetch-sdk.sh && cd examples && make -f hello-world.Makefile
```

This simple command line one-liner will 1) fetch the most recent (at the moment) PocketBook SDK and then compile **hello-world** demo app.

# Links

1. [The PocketBook SDK Documentation, (revision 1.05)](https://github.com/pocketbook-free/InkViewDoc/blob/master/PocketBookSDK.pdf?raw=true).
